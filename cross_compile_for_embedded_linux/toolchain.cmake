# Toolchian file for 64Bits arm
# Depends:
#   sudo apt-get install gcc-aarch64-linux-gnu g++-aarch64-linux-gnu

# This is a MUST
# 必须
# The CMake-identifier of the target platform to build for
# 目标平台的标识符
# 只有这一项被设置后，cmake才认为是在交叉编译, 
# 同时CMAKE_CROSSCOMPILEING被设置为真
set(CMAKE_SYSTEM_NAME Linux)

# 可选
# 目标构架的标识符,如 arm，aarch64, mips
set(CMAKE_SYSTEM_PROCESSOR aarch64)

# 可选
# 如果交叉编译工具链有sysroot，则可设置
#set(CMAKE_SYSROOT /usr/aarch64-linux-gnu)

# 可选
# 编译主机上的临时安装目录
set(CMAKE_STAGING_PREFIX ./usr)

# 必须
# 设置交叉编译所用的编译器，可以是一个绝对路径，也可是一个名字，
# 如果是一个名字，则在标准路径查找
set(CMAKE_C_COMPILER aarch64-linux-gnu-gcc)
set(CMAKE_CXX_COMPILER aarch64-linux-gnu-g++)

# 可选
# 除去sysroot之外的，用于查找软件包、软件库的路径列表,
# 如find_package(), find_library()等
set(CMAKE_FIND_ROOT_PATH /usr/aarch64-linux-gnu)

# 用来说明find_program()如何使用CMAKE_SYSROOT和CMAKE_FIND_ROOT_PATH
#   ONLY:   只有FIND_ROOT_PATH和SYSROOT中的路径被搜索
#   NEVER:  FIND_ROOT_PATH和SYSROOT中的路径被忽略
#   BOTH:   FIND_ROOT_PATH和SYSROOT以及主机系统路径都被搜索
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY only)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE only)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE only)
