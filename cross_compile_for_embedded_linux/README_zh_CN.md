# 示例说明
本示例展示如何使用cmake进行交叉编译。在交叉编译的情况下，cmake使用一个称为
toolchain file的文件来说明编译器和其它工具的路径信息。具体内容参见文件本目录的
toolchain.cmake文件。

# 使用方法
toolchain文件必须在命令行指定或者使用cmake-gui程序选择

cmake -DCMAKE_TOOLCHAIN_FILE=/path/to/toolchain.cmake  /path/to/source

# 相关内容
当交叉编译时，变量CMAKE_CROSSCOMPILING被设为真

# 参考
[cmake官方文档](https://cmake.org/cmake/help/v3.5/manual/cmake-toolchains.7.html?highlight=cmake%20toolchains#introduction)
